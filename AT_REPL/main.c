// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <msp430.h>
#include <driverlib.h>

#include "azure_c_shared_utility/core_msp430.h"
#include "azure_c_shared_utility/platform.h"
#include "azure_c_shared_utility/tickcounter.h"
#include "azure_c_shared_utility/uartio.h"
#include "azure_c_shared_utility/vector.h"
#include "azure_c_shared_utility/xio_stream_filter_ip_address.h"
#include "azure_c_shared_utility/xio_stream_filter_registration_query_response.h"
#include "azure_c_shared_utility/xio_stream_filter_ta_response_code.h"
#include "azure_c_shared_utility/xio_stream_filter_tcp_data_max_size.h"
#include "azure_c_shared_utility/xio_stream_scanner.h"

#define TURBO_BUTTON 0
#define LUDICROUS_SPEED 0

/*
 * (ULP 5.3) Detected printf() operation(s).
 *
 * Recommend moving them to RAM during run time or
 * not using as these are processing/power intensive
 */
//TODO: Confirm decision or address suppression diagnostic message
#pragma diag_suppress 1532

/*
 * controlling expression is constant
 */
//TODO: Confirm decision or address suppression diagnostic message
#pragma diag_suppress 238

/*
 * SIM800 Series_AT Command Manual_V1.09 - Section 1.4.4 - pg. 24
 *
 * The Command line buffer can accept a maximum of 556 characters
 * (counted from the first command without �AT� or �at� prefix).
 * If the characters entered exceeded this number then none of the
 * Command will executed and TA will return "ERROR".
 *
 * strlen("AT") + 556 + strlen("\r");
 */
//#define MAX_AT_COMMAND_SIZE 559

typedef struct RxContext {
	bool awaiting_response;
	bool show_data_prompt;
	VECTOR_HANDLE debug_buffer;
	tickcounter_ms_t sim808_at_command_fail_time_ms;
	tickcounter_ms_t sim808_at_command_send_time_ms;
	XIO_STREAM_FILTER_HANDLE sim808_response_ip_address_filter;
	XIO_STREAM_FILTER_HANDLE sim808_response_registration_query_filter;
	XIO_STREAM_SCANNER_HANDLE sim808_response_scanner;
	XIO_STREAM_FILTER_HANDLE sim808_response_tcp_data_max_size_filter;
	XIO_STREAM_FILTER_HANDLE sim808_ta_response_code_filter;
	size_t state_machine_index;
	XIO_HANDLE sub_io;
	bool sub_io_open;
	TICK_COUNTER_HANDLE tick_counter;
} RxContext;

void
atRepl (
	void * const context_
);

int
msp430_init (
	void
);

void
printError (
	void * context_
);

void
printResponse (
	void * context_,
	const unsigned char * buffer_,
	size_t size_
);

void
processRx (
	void * context_,
	const unsigned char * buffer_,
	size_t size_
);

int
sim808_systemCheck (
	void * context_
);


void
onSubIoCloseComplete (
	void * context_
) {
	RxContext * sim808 = (RxContext *)context_;

	if ( !context_ ) {
		LogError("Underlying XIO called callback without providing an XIO context\n");
	} else {
		sim808->sub_io_open = false;
	}
}


void
onSubIoOpenComplete (
	void * context_,
	IO_OPEN_RESULT open_result_
) {
	RxContext * sim808 = (RxContext *)context_;

	if ( !context_ ) {
		LogError("Underlying XIO called callback without providing an XIO context\n");
	} else {
		switch (open_result_) {
		  case IO_OPEN_OK:
			sim808->sub_io_open = true;
			break;
		  case IO_OPEN_ERROR:
		  case IO_OPEN_CANCELLED:
			sim808->sub_io_open = false;
			break;
		  default:
			LogError("Unsupported XIO open state\n");
			break;
		}
	}
}


int
main (
	int argc,
	char * argv[]
) {
	RxContext sim808 = {
		.awaiting_response = false,
		.debug_buffer = NULL,
		.show_data_prompt = false,
		.sim808_at_command_fail_time_ms = 0,
		.sim808_at_command_send_time_ms = 0,
		.sim808_response_ip_address_filter = NULL,
		.sim808_response_registration_query_filter = NULL,
		.sim808_response_scanner = NULL,
		.sim808_ta_response_code_filter = NULL,
		.state_machine_index = 0,
		.sub_io = NULL,
		.sub_io_open = false,
		.tick_counter = NULL,
	};

	static char answer = 'y';

	for(; tolower(answer) == 'y' ;) {
		// Initialize MSP430FR5969
		if ( msp430_init() != 0 ) {
	        (void)printf("Cannot initialize platform.");
		} else if ( NULL == (sim808.sub_io = xio_create(uartio_get_interface_description(), (const void *)(&(UARTIO_CONFIG){.baud_rate = 9600, .ring_buffer_size = 8}))) ) {
			(void)printf("Failed to create UART XIO layer\n");
		} else {
			for(; tolower(answer) == 'y' ;) {
				if ( NULL == (sim808.tick_counter = tickcounter_create()) ) {
					(void)printf("Failed to create the tick counter\n");
				} else if ( NULL == (sim808.debug_buffer = VECTOR_create(sizeof(uint8_t))) ) {
					(void)printf("Failed to allocate the UART buffer\n");
				} else if ( NULL == (sim808.sim808_ta_response_code_filter = xio_stream_filter_create(xio_stream_filter_get_ta_response_code_interface())) ) {
					(void)printf("Failed to create the ta response code stream filter\n");
				} else if ( NULL == (sim808.sim808_response_ip_address_filter = xio_stream_filter_create(xio_stream_filter_get_ip_address_interface())) ) {
					(void)printf("Failed to create the ip address stream filter\n");
				} else if ( NULL == (sim808.sim808_response_registration_query_filter = xio_stream_filter_create(xio_stream_filter_get_registration_query_response_interface())) ) {
					(void)printf("Failed to create the registration query response stream filter\n");
				} else if ( NULL == (sim808.sim808_response_tcp_data_max_size_filter = xio_stream_filter_create(xio_stream_filter_get_tcp_data_max_size_interface())) ) {
					(void)printf("Failed to create the tcp max data size stream filter\n");
				} else if ( NULL == (sim808.sim808_response_scanner = xio_stream_scanner_create()) ) {
					(void)printf("Failed to create the default stream scanner\n");
				} else if ( xio_stream_scanner_init(sim808.sim808_response_scanner, "SIMCOM_SIM808\r\n", 15) ) {
					(void)printf("Failed to initialize the default stream scanner\n");
				} else if ( xio_open(sim808.sub_io, onSubIoOpenComplete, &sim808, processRx, &sim808, printError, &sim808) ) {
					(void)printf("Failed to open the UART XIO layer\n");
				} else {
					// (R)ead (E)val (P)rint (L)oop for AT commands
					(void)atRepl(&sim808);
					if ( xio_close(sim808.sub_io, onSubIoCloseComplete, &sim808) ) {
						(void)printf("Failed to close the XIO layer\n");
					} else {
						(void)printf("The connection to the Sim808 has been closed.\n");
					}
				}

				(void)xio_stream_scanner_destroy(sim808.sim808_response_scanner);
				(void)xio_stream_filter_destroy(sim808.sim808_response_tcp_data_max_size_filter);
				(void)xio_stream_filter_destroy(sim808.sim808_response_registration_query_filter);
				(void)xio_stream_filter_destroy(sim808.sim808_response_ip_address_filter);
				(void)xio_stream_filter_destroy(sim808.sim808_ta_response_code_filter);
				(void)VECTOR_destroy(sim808.debug_buffer);
				(void)tickcounter_destroy(sim808.tick_counter);

				// Prompt user to reopen
				(void)fflush(stdin);
				(void)printf("Would you like to reopen (y/N)? ");
				answer = getc(stdin);
			}
			(void)xio_destroy(sim808.sub_io);
			sim808.state_machine_index = 0;
			(void)platform_deinit();
			(void)printf("Resources have been released.\n");
		}

		// Prompt user for restart
		(void)fflush(stdin);
		(void)printf("Would you like to restart (y/N)? ");
		answer = getc(stdin);
	}
	(void)printf("OK to halt debugger.");

	return 0;
}


/******************************************************************************
 * (R)ead (E)val (P)rint (L)oop for AT commands
 *
 * This REPL is used to manually test AT commands
 *
 ******************************************************************************/
void
atRepl (
	void * const context_
) {
	if ( !context_ ) { return; }

	RxContext * sim808 = (RxContext *)context_;
  #if !ENABLE_STATE_MACHINE
	static char buffer[128];
	static char * upper_case = buffer;

	(void)printf("|>>>> Begin REPL <<<<|\n");
	(void)printf("MCLK (MCU) Hz: %lu\n", CS_getMCLK());
	(void)printf("SMCLK (UART) Hz: %lu\n", CS_getSMCLK());
	(void)printf("ACLK/%u (Timer A3) Hz: %lu\n", (1 << 4), (CS_getACLK() >> 4));
   #if DEBUG_CLOCK
	(void)DEBUG_test_clock(1000);
   #endif
  #endif

	for(;;) {
		if ( 14 > sim808->state_machine_index ) {
			if ( sim808_systemCheck(context_) ) {
				if ( printError ) { printError(context_); }  // state machine failed
			}
		} else if ( !sim808->awaiting_response ) {
			if ( xio_stream_filter_full(sim808->sim808_response_registration_query_filter) ) {
				RegistrationQueryResponse * rqr = (RegistrationQueryResponse *)xio_stream_filter_contents(sim808->sim808_response_registration_query_filter, NULL);
				(void)printf("Registration details:\n\tn: %c\n\tstat: %c\n\tlac: \"%s\"\n\tci: \"%s\"\n", rqr->n, rqr->stat, rqr->lac, rqr->ci);
				xio_stream_filter_reset(sim808->sim808_response_registration_query_filter);
			}

			if ( xio_stream_filter_full(sim808->sim808_response_tcp_data_max_size_filter) ) {
				(void)printf("Max TCP data size: %u\n", *(size_t *)xio_stream_filter_contents(sim808->sim808_response_tcp_data_max_size_filter, NULL));
				xio_stream_filter_reset(sim808->sim808_response_tcp_data_max_size_filter);
			}

			// Prompt user for AT command
			if ( sim808->show_data_prompt ) {
				(void)printf("data> ");
			} else {
				(void)printf("Please enter an AT command (or \"quit\"): ");
			}

			(void)fflush(stdin);
			(void)scanf("%s", buffer);

			if ( sim808->show_data_prompt ) {
				sim808->show_data_prompt = false;
			} else {
			  #pragma diag_push
			  /*
			   * (ULP 2.1) Detected SW delay loop using empty loop.
			   *
			   * Recommend using a timer module instead
			   */
			  //TODO: Confirm decision or address suppression diagnostic message
			  #pragma diag_suppress 1527
				for(upper_case = buffer ; *upper_case ; *upper_case++ = toupper(*upper_case));  // Uppercase user input
			  #pragma diag_pop
				// Check for special condition(s)
				if ( !strcmp("QUIT", buffer) ) { break; }
			}

			// Send AT command
			(void)printf("Sending \"%s\\r\" to UART1\n", buffer);
			(void)strcat(buffer, "\r");
			if ( xio_send(sim808->sub_io, buffer, strlen(buffer), NULL, NULL) ) {
				(void)printf("Failed to send buffer the XIO layer\n");
			} else {
				sim808->awaiting_response = true;
			}
		}
		xio_dowork(sim808->sub_io);
	}
}


//*****************************************************************************
//
//!/*CLOCK_INIT*/
//!Brief Clock_Init function will assign clock for ACLK,SMCLK,MCLK
//!
//           MSP430FR59x
//         ---------------
//     /|\|               |
//      | |               |-LFXIN
//      --|RST            |-LFXOUT
//        |               |
//        |               |-HFXIN
//        |               |-HFXOUT
//        |               |
//        |           	  |---> LED
//        |               |---> ACLK = 32768Hz
//        |               |---> SMCLK = 2MHz
//! \return None

//*****************************************************************************
int
msp430_init (
	void
) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
  #if TURBO_BUTTON
   #if LUDICROUS_SPEED
	(void)CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_6);
   #else
	(void)CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_4);
   #endif
	(void)CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_2);
  #else
	// Default values
	(void)CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_6);
	(void)CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_8);
  #endif

	// Initialize Port A
	PAOUT = 0x00;  // Set Port A to LOW
	PADIR = 0x00;  // Set Port A to INPUT

	// Initialize Port B
	PBOUT = 0x00;  // Set Port B to LOW
	PBDIR = 0x00;  // Set Port B to OUTPUT

	PM5CTL0 &= ~LOCKLPM5;		// Disable the GPIO power-on default high-impedance mode to
								// activate previously configured port settingsGS (affects RTC)
	__bis_SR_register(GIE);		// Enable Global Interrupt

	return platform_init();
}


void
printError (
	void *context_
) {
	RxContext * sim808 = (RxContext *)context_;

	if ( !context_ ) {
		(void)printf("Failed to pass context to error function\n");
	} else {
		(void)printf("UART error!\n\tAwaiting response? %s\n\tShow data prompt? %s\n\tRX buffer location: %p\n\tAT send time: %lu\n\tAT fail time: %lu\n\tState machine index: %u\n", (sim808->awaiting_response ? "YES" : "NO"), (sim808->show_data_prompt ? "YES" : "NO"), VECTOR_front(sim808->debug_buffer), sim808->sim808_at_command_send_time_ms, sim808->sim808_at_command_fail_time_ms, sim808->state_machine_index);
		for (;;); // stop the program with stack intact
	}
}


void
printResponse (
	void * context_,
	const unsigned char * buffer_,
	size_t size_
) {
	RxContext * sim808 = (RxContext *)context_;
	tick_t this_ticks = timer_a3_ticks();
	tickcounter_ms_t current_ms;

	if ( !context_ ) {
		(void)printf("Failed to pass context to print function\n");
	} else if ( !buffer_ ) {
		(void)printError(context_);
	} else if ( !size_ ) {
		(void)printError(context_);
	} else if ( tickcounter_get_current_ms(sim808->tick_counter, &current_ms) ) {
		(void)printError(context_);
	} else {
		(void)printf("Current milliseconds: %lu\nResponse:\n%s", current_ms, (const char *)buffer_);
		(void)printf("Ticks count: %lu\n\n", this_ticks);
	}
}


//*****************************************************************************
//! \brief Process bytes that have been sent to the UART1 ring buffer.
//*****************************************************************************
void
processRx (
	void * context_,
	const unsigned char * buffer_,
	size_t size_
) {
	if ( !context_ ) { return; }

	RxContext * sim808 = (RxContext *)context_;

	if ( VECTOR_push_back(sim808->debug_buffer, (const void *)buffer_, size_) ) {
		if ( printError ) { printError(context_); }  // vector out of memory
	} else if ( xio_stream_filter_process_stream(sim808->sim808_ta_response_code_filter, buffer_, size_) ) {
		if ( printError ) { printError(context_); }  // bad stream filter handle
	} else if ( xio_stream_filter_process_stream(sim808->sim808_response_registration_query_filter, buffer_, size_) ) {
		if ( printError ) { printError(context_); }  // bad stream filter handle
	} else if ( xio_stream_filter_process_stream(sim808->sim808_response_ip_address_filter, buffer_, size_) ) {
		if ( printError ) { printError(context_); }  // bad stream filter handle
	} else if ( xio_stream_filter_process_stream(sim808->sim808_response_tcp_data_max_size_filter, buffer_, size_) ) {
		if ( printError ) { printError(context_); }  // bad stream filter handle
	} else if ( xio_stream_scanner_process_stream(sim808->sim808_response_scanner, buffer_, size_) ) {
		if ( printError ) { printError(context_); }  // bad stream scanner handle
	} else if ( sim808->state_machine_index < 14 ) {
		// Clear the vector during system check
	} else {
		// Do additional processing here...
		static const uint8_t NULL_TERMINATOR[] = "\0";

		if ( !(sim808->awaiting_response = !xio_stream_filter_full(sim808->sim808_ta_response_code_filter)) ) {
			if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
				if ( printError ) { printError(context_); }  // unable to reset filter
			} else if ( VECTOR_push_back(sim808->debug_buffer, NULL_TERMINATOR, 1) ) {
				if ( printError ) { printError(context_); }  // no more space for vector
			} else if ( printResponse ) {
				(void)printResponse(context_, VECTOR_front(sim808->debug_buffer), VECTOR_size(sim808->debug_buffer));
			}
			(void)VECTOR_clear(sim808->debug_buffer);
		}
	}
}


int
sim808_systemCheck (
	void * context_
) {
	const uint8_t TA_OK = 0x30;

	RxContext * sim808 = (RxContext *)context_;
	tickcounter_ms_t current_ms = 0;
	size_t error = 0;

	if ( !context_ ) {
		error = __LINE__;
	} else if ( tickcounter_get_current_ms(sim808->tick_counter, &current_ms) ) {
		error = __LINE__;
	} else {
		switch ( sim808->state_machine_index ) {
		  case 0:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(sim808->sim808_response_scanner, "OK\r\n", (sizeof("OK\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT\r", (sizeof("AT\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			} else if ( xio_stream_scanner_matched(sim808->sim808_response_scanner) ) {
				++sim808->state_machine_index;
				sim808->sim808_at_command_send_time_ms = 0;
			// Test retry condition
			} else if ( 100 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				/*
				 * Resend every 100ms until valid response (SIM800 Series_AT Command Manual_V1.09 - pg. 160)
				 *
				 * An interval time of 100ms more is necessary between waking
				 * characters and following AT commands, otherwise the waking
				 * characters will not be discarded completely, and messy codes
				 * will be produced which may leads to UART baud rate re-adaptation.
				 */
				sim808->sim808_at_command_send_time_ms = 0;
			}
			break;
		  case 1:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "ATV0E0\r", (sizeof("ATV0E0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 2:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(sim808->sim808_response_scanner, "SIMCOM_SIM808\r\n", (sizeof("SIMCOM_SIM808\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+GMM\r", (sizeof("AT+GMM\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_scanner_matched(sim808->sim808_response_scanner)
			  && xio_stream_filter_full(sim808->sim808_ta_response_code_filter)
			) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 3:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+IPR=0\r", (sizeof("AT+IPR=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 4:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CREG=0\r", (sizeof("AT+CREG=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 5:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CGREG=0\r", (sizeof("AT+CGREG=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 6:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+EXUNSOL=\"SQ\",0\r", (sizeof("AT+EXUNSOL=\"SQ\",0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 7:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT&W\r", (sizeof("AT&W\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 8:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+GSMBUSY=1\r", (sizeof("AT+GSMBUSY=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 9:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(sim808->sim808_response_registration_query_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CREG?\r", (sizeof("AT+CREG?\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
					sim808->sim808_at_command_fail_time_ms = 0;
				}
			// Test exit criteria
			} else if ( !sim808->sim808_at_command_fail_time_ms
			  && xio_stream_filter_full(sim808->sim808_ta_response_code_filter)
			) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					RegistrationQueryResponse * rqr = (RegistrationQueryResponse *)xio_stream_filter_contents(sim808->sim808_response_registration_query_filter, NULL);
					if ( xio_stream_filter_full(sim808->sim808_response_registration_query_filter)
					  && ((0x31 == rqr->stat) || (0x35 == rqr->stat))
					) {
						++sim808->state_machine_index;
						sim808->sim808_at_command_send_time_ms = 0;
					} else {
						sim808->sim808_at_command_fail_time_ms = current_ms;
						sim808->sim808_at_command_send_time_ms = current_ms;
					}
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test retry condition
			} else if ( (0 != sim808->sim808_at_command_fail_time_ms)
			  && (500 <= (current_ms - sim808->sim808_at_command_fail_time_ms))
			) {
				// Expected to fail while the GSM is resolving connection - Retry OK
				sim808->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 10:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CGATT=0\r", (sizeof("AT+CGATT=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 10000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 11:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CGATT=1\r", (sizeof("AT+CGATT=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
				}
			// Test timeout condition
			} else if ( 10000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 12:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(sim808->sim808_response_registration_query_filter) ) {
						error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CGREG?\r", (sizeof("AT+CGREG?\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
					sim808->sim808_at_command_fail_time_ms = 0;
				}
			// Test exit criteria
			} else if ( !sim808->sim808_at_command_fail_time_ms
			  && xio_stream_filter_full(sim808->sim808_ta_response_code_filter)
			) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					RegistrationQueryResponse * rqr = (RegistrationQueryResponse *)xio_stream_filter_contents(sim808->sim808_response_registration_query_filter, NULL);
					if ( xio_stream_filter_full(sim808->sim808_response_registration_query_filter)
					  && ((0x31 == rqr->stat) || (0x35 == rqr->stat))
					) {
						++sim808->state_machine_index;
						sim808->sim808_at_command_send_time_ms = 0;
					} else {
						sim808->sim808_at_command_fail_time_ms = current_ms;
						sim808->sim808_at_command_send_time_ms = current_ms;
					}
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test retry condition
			} else if ( (0 != sim808->sim808_at_command_fail_time_ms)
			  && (500 <= (current_ms - sim808->sim808_at_command_fail_time_ms))
			) {
				// Expected to fail while the GSM is resolving connection - Retry OK
				sim808->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 13:
			if ( 0 == sim808->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(sim808->sub_io, "AT+CGEREP=0\r", (sizeof("AT+CGEREP=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					sim808->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(sim808->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(sim808->sim808_ta_response_code_filter, NULL) ) {
					++sim808->state_machine_index;
					sim808->sim808_at_command_send_time_ms = 0;

					// Clean up for REPL
					(void)VECTOR_clear(sim808->debug_buffer);
					if ( xio_stream_filter_reset(sim808->sim808_ta_response_code_filter) ) {
						error = __LINE__;
					} else if ( xio_stream_filter_reset(sim808->sim808_response_registration_query_filter) ) {
						error = __LINE__;
					} else if ( xio_stream_filter_reset(sim808->sim808_response_ip_address_filter) ) {
						error = __LINE__;
					} else if ( xio_stream_filter_reset(sim808->sim808_response_tcp_data_max_size_filter) ) {
						error = __LINE__;
					} else if ( xio_stream_scanner_init(sim808->sim808_response_scanner, "\r", 1) ) {
						error = __LINE__;
					} else {
						// Any additional clean up before REPL start
					}
				} else {
					sim808->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - sim808->sim808_at_command_send_time_ms) ) {
				sim808->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		}
	}

	return error;
}


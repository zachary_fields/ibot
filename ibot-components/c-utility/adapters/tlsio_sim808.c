// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include "azure_c_shared_utility/tlsio_sim808.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "azure_c_shared_utility/tickcounter.h"
#include "azure_c_shared_utility/uartio.h"
#include "azure_c_shared_utility/vector.h"
#include "azure_c_shared_utility/xio_stream_filter_ip_address.h"
#include "azure_c_shared_utility/xio_stream_filter_registration_query_response.h"
#include "azure_c_shared_utility/xio_stream_filter_ta_response_code.h"
#include "azure_c_shared_utility/xio_stream_filter_tcp_data_max_size.h"
#include "azure_c_shared_utility/xio_stream_filter_tcp_traffic.h"
#include "azure_c_shared_utility/xio_stream_scanner.h"

/*
 * SIM800 Series_AT Command Manual_V1.09 - Section 1.4.4 - pg. 24
 *
 * The Command line buffer can accept a maximum of 556 characters
 * (counted from the first command without �AT� or �at� prefix).
 * If the characters entered exceeded this number then none of the
 * Command will executed and TA will return "ERROR".
 *
 * strlen("AT") + 556;
 */
//#define AT_COMMAND_SIZE_MAX 558

#define TA_OK '0'

typedef enum TlsIoState {
	TLSIO_CLOSED,
	TLSIO_CLOSE_REQUESTED,
	TLSIO_OPEN,
	TLSIO_OPEN_REQUESTED,
	TLSIO_OPEN_REQUESTED_ATTACH_TO_NETWORK,
	TLSIO_OPEN_REQUESTED_OPEN_TLS_CHANNEL,
	TLSIO_OPEN_REQUESTED_SYSTEM_CHECK,
} TlsIoState;

typedef struct TlsIoContext {
	ON_BYTES_RECEIVED on_bytes_received;
	void * on_bytes_received_context;
	ON_IO_CLOSE_COMPLETE on_io_close_complete;
	void * on_io_close_complete_context;
	ON_IO_ERROR on_io_error;
	void * on_io_error_context;
	ON_IO_OPEN_COMPLETE on_io_open_complete;
	void * on_io_open_complete_context;
	ON_SEND_COMPLETE on_send_complete;
	void * on_send_complete_context;
	char * sim808_access_point_name;
	tickcounter_ms_t sim808_at_command_fail_time_ms;
	tickcounter_ms_t sim808_at_command_send_time_ms;
	XIO_STREAM_FILTER_HANDLE sim808_response_ip_address_filter;
	XIO_STREAM_FILTER_HANDLE sim808_response_registration_query_filter;
	XIO_STREAM_SCANNER_HANDLE sim808_response_scanner;
	XIO_STREAM_SCANNER_HANDLE sim808_response_scanner_alternate;
	XIO_STREAM_FILTER_HANDLE sim808_response_tcp_data_max_size_filter;
	XIO_STREAM_FILTER_HANDLE sim808_response_tcp_traffic_filter;
	XIO_STREAM_FILTER_HANDLE sim808_ta_response_code_filter;
	size_t sim808_tcp_data_max_size;
	size_t state_machine_index;
	XIO_HANDLE sub_io;
	bool sub_io_open;
	TICK_COUNTER_HANDLE tick_counter;
	bool tlsio_clear_to_send;
	TLSIO_CONFIG tlsio_config;
	TlsIoState tlsio_state;
} TlsIoContext;

void
onUnderlyingIoCloseComplete (
	void * context_
);

void
onUnderlyingIoOpenComplete (
	void * context_,
	IO_OPEN_RESULT open_result_
);

void
processUnderlyingIo (
	void * context_,
	const unsigned char * buffer_,
	size_t size_
);

void *
tlsio_sim808_cloneoption (
	const char * option_name_,
	const void * option_value_
);

void
tlsio_sim808_destroyoption (
	const char * option_name_,
	const void * option_value_
);

static const
IO_INTERFACE_DESCRIPTION _tlsio_sim808_interface_description = {
	.concrete_io_close = tlsio_sim808_close,
	.concrete_io_create = tlsio_sim808_create,
	.concrete_io_destroy = tlsio_sim808_destroy,
	.concrete_io_dowork = tlsio_sim808_dowork,
	.concrete_io_open = tlsio_sim808_open,
	.concrete_io_retrieveoptions = tlsio_sim808_retrieveoptions,
	.concrete_io_send = tlsio_sim808_send,
	.concrete_io_setoption = tlsio_sim808_setoption
};

static XIO_HANDLE _singleton = NULL;


inline
char *
alloc_itoa_base10 (
	const int integer_
) {
	const size_t ASCII_INTEGER_OFFSET = 0x30;

	char * ascii = NULL;
	int exponent = 1;
	size_t abs_integer = abs(integer_), length = 0, test = 1;

	// Determine length required for string
	for (; integer_ > (test *= 10) ; ++exponent);
	length = (exponent + 1);  // add 1 for NULL terminator

	if ( integer_ < 0 ) { ++length; }  // add 1 for negative sign
	if ( NULL != (ascii = (char *)malloc(length)) ) {
		if ( integer_ < 0 ) { ascii[0] = '-'; }
		ascii[--length] = '\0';
		for (; exponent > 0 ; --exponent, abs_integer /= 10) {
			ascii[--length] = ((abs_integer % 10) + ASCII_INTEGER_OFFSET);
		}
	}

	return ascii;
}


inline
int
tlsioCloseRequested (
	void * context_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;
	tickcounter_ms_t current_ms = 0;
	size_t error = 0;

	if ( !context_ ) {
		error = __LINE__;
	} else if ( tickcounter_get_current_ms(tlsio->tick_counter, &current_ms) ) {
		error = __LINE__;
	} else {
		switch ( tlsio->state_machine_index ) {
		  case 38:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPCLOSE\r", (sizeof("AT+CIPCLOSE\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				// This is a best effort attempt, the actual response code is irrelevant
				++tlsio->state_machine_index;
				tlsio->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 39:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_scanner_init(tlsio->sim808_response_scanner, "\r\nSHUT OK\r\n", (sizeof("\r\nSHUT OK\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(tlsio->sim808_response_scanner_alternate, "\r\nERROR\r\n", (sizeof("\r\nERROR\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSHUT\r", (sizeof("AT+CIPSHUT\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner) ) {
				++tlsio->state_machine_index;
				tlsio->sim808_at_command_send_time_ms = 0;
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner_alternate) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			// Test timeout condition
			} else if ( 65000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 40:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CGATT=0\r", (sizeof("AT+CGATT=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
					if ( xio_close(tlsio->sub_io, onUnderlyingIoCloseComplete, context_) ) {
						error = __LINE__;
					} else {
						LogInfo("Successfully requested close of underlying XIO layer.");
					}
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 10000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		}
	}

	return error;
}


inline
int
tlsioOpenRequested_attachToNetwork (
	void * context_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;
	tickcounter_ms_t current_ms = 0;
	size_t error = 0;

	if ( !context_ ) {
		error = __LINE__;
	} else if ( tickcounter_get_current_ms(tlsio->tick_counter, &current_ms) ) {
		error = __LINE__;
	} else {
		switch ( tlsio->state_machine_index ) {
		  case 8:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+GSMBUSY=1\r", (sizeof("AT+GSMBUSY=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 9:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(tlsio->sim808_response_registration_query_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CREG?\r", (sizeof("AT+CREG?\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
					tlsio->sim808_at_command_fail_time_ms = 0;
				}
			// Test exit criteria
			} else if ( !tlsio->sim808_at_command_fail_time_ms
			  && xio_stream_filter_full(tlsio->sim808_ta_response_code_filter)
			) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					RegistrationQueryResponse * rqr = (RegistrationQueryResponse *)xio_stream_filter_contents(tlsio->sim808_response_registration_query_filter, NULL);
					if ( xio_stream_filter_full(tlsio->sim808_response_registration_query_filter)
					  && ((0x31 == rqr->stat) || (0x35 == rqr->stat))
					) {
						++tlsio->state_machine_index;
						tlsio->sim808_at_command_send_time_ms = 0;
					} else {
						tlsio->sim808_at_command_fail_time_ms = current_ms;
						tlsio->sim808_at_command_send_time_ms = current_ms;
					}
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test retry condition
			} else if ( (0 != tlsio->sim808_at_command_fail_time_ms)
			  && (500 <= (current_ms - tlsio->sim808_at_command_fail_time_ms))
			) {
				// Expected to fail while the GSM is resolving connection - Retry OK
				tlsio->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 10:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CGATT=0\r", (sizeof("AT+CGATT=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 10000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 11:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CGATT=1\r", (sizeof("AT+CGATT=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
				}
			// Test timeout condition
			} else if ( 10000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 12:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(tlsio->sim808_response_registration_query_filter) ) {
						error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CGREG?\r", (sizeof("AT+CGREG?\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
					tlsio->sim808_at_command_fail_time_ms = 0;
				}
			// Test exit criteria
			} else if ( !tlsio->sim808_at_command_fail_time_ms
			  && xio_stream_filter_full(tlsio->sim808_ta_response_code_filter)
			) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					RegistrationQueryResponse * rqr = (RegistrationQueryResponse *)xio_stream_filter_contents(tlsio->sim808_response_registration_query_filter, NULL);
					if ( xio_stream_filter_full(tlsio->sim808_response_registration_query_filter)
					  && ((0x31 == rqr->stat) || (0x35 == rqr->stat))
					) {
						++tlsio->state_machine_index;
						tlsio->sim808_at_command_send_time_ms = 0;
					} else {
						tlsio->sim808_at_command_fail_time_ms = current_ms;
						tlsio->sim808_at_command_send_time_ms = current_ms;
					}
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test retry condition
			} else if ( (0 != tlsio->sim808_at_command_fail_time_ms)
			  && (500 <= (current_ms - tlsio->sim808_at_command_fail_time_ms))
			) {
				// Expected to fail while the GSM is resolving connection - Retry OK
				tlsio->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 13:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CGEREP=0\r", (sizeof("AT+CGEREP=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
					tlsio->tlsio_state = TLSIO_OPEN_REQUESTED_OPEN_TLS_CHANNEL;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		}
	}

	return error;
}


inline
int
tlsioOpenRequested_openTlsChannel (
	void * context_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;
	tickcounter_ms_t current_ms = 0;
	size_t error = 0;

	if ( !context_ ) {
		error = __LINE__;
	} else if ( tickcounter_get_current_ms(tlsio->tick_counter, &current_ms) ) {
		error = __LINE__;
	} else {
		switch ( tlsio->state_machine_index ) {
		  case 14:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_scanner_init(tlsio->sim808_response_scanner, "\r\nSHUT OK\r\n", (sizeof("\r\nSHUT OK\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSHUT\r", (sizeof("AT+CIPSHUT\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner) ) {
				++tlsio->state_machine_index;
				tlsio->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 65000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 15:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPMUX=0\r", (sizeof("AT+CIPMUX=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 16:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPCSGP=1\r", (sizeof("AT+CIPCSGP=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 17:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPDPDP=1,10,3\r", (sizeof("AT+CIPDPDP=1,10,3\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 18:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPRDTIMER=2000,3500\r", (sizeof("AT+CIPRDTIMER=2000,3500\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 19:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPQSEND=0\r", (sizeof("AT+CIPQSEND=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 20:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPATS=0\r", (sizeof("AT+CIPATS=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 21:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPMODE=0\r", (sizeof("AT+CIPMODE=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 22:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPRXGET=0\r", (sizeof("AT+CIPRXGET=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 23:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPTKA=0\r", (sizeof("AT+CIPTKA=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 24:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPHEAD=0\r", (sizeof("AT+CIPHEAD=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 25:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSHOWTP=0\r", (sizeof("AT+CIPSHOWTP=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 26:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSRIP=0\r", (sizeof("AT+CIPSRIP=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 27:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSPRT=1\r", (sizeof("AT+CIPSPRT=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 28:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				const char at_command_prefix[] = "AT+CSTT=\"";

				size_t at_command_size = sizeof(at_command_prefix); // includes NULL terminator
				at_command_size += strlen(tlsio->sim808_access_point_name);
				at_command_size += 2; // `\"\r`
				char * at_command = (char *)malloc(at_command_size);

				if ( at_command != strcpy(at_command, at_command_prefix) ) {
					error = __LINE__;
				} else if ( at_command != strcat(at_command, tlsio->sim808_access_point_name) ) {
					error = __LINE__;
				} else if ( at_command != strcat(at_command, "\"\r") ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, at_command, strlen(at_command), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}

				(void)free(at_command);
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 29:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSCONT\r", (sizeof("AT+CIPSCONT\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 30:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIICR\r", (sizeof("AT+CIICR\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 85000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 31:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSSL=1\r", (sizeof("AT+CIPSSL=1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 32:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+SSLOPT=0,1\r", (sizeof("AT+SSLOPT=0,1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 33:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+SSLOPT=1,1\r", (sizeof("AT+SSLOPT=1,1\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 34:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_response_ip_address_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIFSR\r", (sizeof("AT+CIFSR\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_response_ip_address_filter) ) {
				++tlsio->state_machine_index;
				tlsio->sim808_at_command_send_time_ms = 0;
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 35:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				const char at_command_prefix[] = "AT+CIPSTART=\"TCP\",\"";

				const char * port = alloc_itoa_base10(tlsio->tlsio_config.port);
				size_t at_command_size = sizeof(at_command_prefix);  // includes NULL terminator
				at_command_size += strlen(tlsio->tlsio_config.hostname);
				at_command_size += 2; // `\",`
				at_command_size += strlen(port);
				at_command_size += 1; // `\r`
				char * at_command = (char *)malloc(at_command_size);

				if ( at_command != strcpy(at_command, at_command_prefix) ) {
					error = __LINE__;
				} else if ( at_command != strcat(at_command, tlsio->tlsio_config.hostname) ) {
					error = __LINE__;
				} else if ( at_command != strcat(at_command, "\",") ) {
					error = __LINE__;
				} else if ( at_command != strcat(at_command, port) ) {
					error = __LINE__;
				} else if ( at_command != strcat(at_command, "\r") ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(tlsio->sim808_response_scanner, "\r\nCONNECT OK\r\n", (sizeof("\r\nCONNECT OK\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(tlsio->sim808_response_scanner_alternate, "\r\nCONNECT FAIL\r\n", (sizeof("\r\nCONNECT FAIL\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, at_command, strlen(at_command), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}

				(void)free(at_command);
				(void)free((void *)port);
			// Test exit criteria
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner) ) {
				if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
					if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
						++tlsio->state_machine_index;
						tlsio->sim808_at_command_send_time_ms = 0;
					} else {
						tlsio->sim808_at_command_send_time_ms = 0;
						error = __LINE__;
					}
				}
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner_alternate) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			// Test timeout condition
			} else if ( 160000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 36:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_filter_reset(tlsio->sim808_response_tcp_data_max_size_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSEND?\r", (sizeof("AT+CIPSEND?\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					if ( xio_stream_filter_full(tlsio->sim808_response_tcp_data_max_size_filter) ) {
						tlsio->sim808_tcp_data_max_size = *(size_t *)xio_stream_filter_contents(tlsio->sim808_response_tcp_data_max_size_filter, NULL);
						++tlsio->state_machine_index;
						tlsio->sim808_at_command_send_time_ms = 0;
					} else {
						tlsio->sim808_at_command_fail_time_ms = current_ms;
						tlsio->sim808_at_command_send_time_ms = current_ms;
					}
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 645000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 37:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_scanner_init(tlsio->sim808_response_scanner, "\r\n>", (sizeof("\r\n>") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CIPSEND\r", (sizeof("AT+CIPSEND\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner) ) {
				++tlsio->state_machine_index;
				tlsio->sim808_at_command_send_time_ms = 0;

				// TLS channel has been successfully opened
				tlsio->tlsio_state = TLSIO_OPEN;
				if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_OK); }
			// Test timeout condition
			} else if ( 645000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		}
	}

	return error;
}


inline
int
tlsioOpenRequested_systemCheck (
	void * context_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;
	tickcounter_ms_t current_ms = 0;
	size_t error = 0;

	if ( !context_ ) {
		error = __LINE__;
	} else if ( tickcounter_get_current_ms(tlsio->tick_counter, &current_ms) ) {
		error = __LINE__;
	} else {
		switch ( tlsio->state_machine_index ) {
		  case 0:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(tlsio->sim808_response_scanner, "OK\r\n", (sizeof("OK\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT\r", (sizeof("AT\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner) ) {
				++tlsio->state_machine_index;
				tlsio->sim808_at_command_send_time_ms = 0;
			// Test retry condition
			} else if ( 100 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				/*
				 * Resend every 100ms until valid response (SIM800 Series_AT Command Manual_V1.09 - pg. 160)
				 *
				 * An interval time of 100ms more is necessary between waking
				 * characters and following AT commands, otherwise the waking
				 * characters will not be discarded completely, and messy codes
				 * will be produced which may leads to UART baud rate re-adaptation.
				 */
				tlsio->sim808_at_command_send_time_ms = 0;
			}
			break;
		  case 1:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "ATV0E0\r", (sizeof("ATV0E0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 2:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_stream_scanner_init(tlsio->sim808_response_scanner, "SIMCOM_SIM808\r\n", (sizeof("SIMCOM_SIM808\r\n") - 1)) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+GMM\r", (sizeof("AT+GMM\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_scanner_matched(tlsio->sim808_response_scanner)
			  && xio_stream_filter_full(tlsio->sim808_ta_response_code_filter)
			) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 3:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+IPR=0\r", (sizeof("AT+IPR=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 4:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CREG=0\r", (sizeof("AT+CREG=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 5:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+CGREG=0\r", (sizeof("AT+CGREG=0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 6:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT+EXUNSOL=\"SQ\",0\r", (sizeof("AT+EXUNSOL=\"SQ\",0\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		  case 7:
			if ( 0 == tlsio->sim808_at_command_send_time_ms ) {
				if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
					error = __LINE__;
				} else if ( xio_send(tlsio->sub_io, "AT&W\r", (sizeof("AT&W\r") - 1), NULL, NULL) ) {
					error = __LINE__;
				} else {
					tlsio->sim808_at_command_send_time_ms = current_ms;
				}
			// Test exit criteria
			} else if ( xio_stream_filter_full(tlsio->sim808_ta_response_code_filter) ) {
				if ( TA_OK == *(uint8_t *)xio_stream_filter_contents(tlsio->sim808_ta_response_code_filter, NULL) ) {
					++tlsio->state_machine_index;
					tlsio->sim808_at_command_send_time_ms = 0;
					tlsio->tlsio_state = TLSIO_OPEN_REQUESTED_ATTACH_TO_NETWORK;
				} else {
					tlsio->sim808_at_command_send_time_ms = 0;
					error = __LINE__;
				}
			// Test timeout condition
			} else if ( 1000 <= (current_ms - tlsio->sim808_at_command_send_time_ms) ) {
				tlsio->sim808_at_command_send_time_ms = 0;
				error = __LINE__;
			}
			break;
		}
	}

	return error;
}


void
onUnderlyingIoCloseComplete (
	void * context_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;

	if ( !context_ ) {
		LogError("Underlying XIO called callback without providing an XIO context\n");
	} else {
		tlsio->on_bytes_received = NULL;
		tlsio->on_bytes_received_context = NULL;
		tlsio->on_io_error = NULL;
		tlsio->on_io_error_context = NULL;
		tlsio->sub_io_open = false;
		tlsio->tlsio_state = TLSIO_CLOSED;
		if ( tlsio->on_io_close_complete ) { tlsio->on_io_close_complete(tlsio->on_io_close_complete_context); }
		tlsio->on_io_close_complete_context = NULL;
		tlsio->on_io_close_complete = NULL;
	}
}


void
onUnderlyingIoOpenComplete (
	void * context_,
	IO_OPEN_RESULT open_result_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;

	if ( !context_ ) {
		LogError("Underlying XIO called callback without providing an XIO context\n");
	} else {
		switch (open_result_) {
		  case IO_OPEN_OK:
			tlsio->sub_io_open = true;
			break;
		  case IO_OPEN_ERROR:
		  case IO_OPEN_CANCELLED:
			tlsio->sub_io_open = false;
			break;
		  default:
			LogError("Unsupported XIO open state\n");
			break;
		}
	}
}


//*****************************************************************************
//! \brief Process bytes and reconstruct response from the lower layer XIO
//*****************************************************************************
void
processUnderlyingIo (
	void * context_,
	const uint8_t * buffer_,
	size_t size_
) {
	TlsIoContext * tlsio = (TlsIoContext *)context_;

	if ( !context_ ) {
		LogError("Underlying XIO called callback without providing an XIO context\n");
	} else if ( !buffer_ ) {
		LogError("Underlying XIO called callback without providing a buffer to process\n");
	} else if ( !size_ ) {
		LogInfo("Underlying XIO called callback with an empty buffer\n");
	} else {
		uint8_t * tcp_traffic_buffer = NULL;
		size_t tcp_traffic_buffer_size = 0;

		switch (tlsio->tlsio_state) {
		  case TLSIO_OPEN_REQUESTED_SYSTEM_CHECK:
			if ( xio_stream_filter_process_stream(tlsio->sim808_ta_response_code_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response code filter failed to process bytes\n");
			} else if ( xio_stream_scanner_process_stream(tlsio->sim808_response_scanner, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response scanner failed to process bytes\n");
			} else {
				// Do any additional stream processing required for this state
			}
			break;
		  case TLSIO_OPEN_REQUESTED_ATTACH_TO_NETWORK:
			if ( xio_stream_filter_process_stream(tlsio->sim808_ta_response_code_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response code filter failed to process bytes\n");
			} else if ( xio_stream_filter_process_stream(tlsio->sim808_response_registration_query_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: Registration filter failed to process bytes\n");
			} else {
				// Do any additional stream processing required for this state
			}
			break;
		  case TLSIO_OPEN_REQUESTED_OPEN_TLS_CHANNEL:
			if ( xio_stream_filter_process_stream(tlsio->sim808_ta_response_code_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response code filter failed to process bytes\n");
			} else if ( xio_stream_filter_process_stream(tlsio->sim808_response_ip_address_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: IP address filter failed to process bytes\n");
			} else if ( xio_stream_filter_process_stream(tlsio->sim808_response_tcp_data_max_size_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: IP address filter failed to process bytes\n");
			} else if ( xio_stream_scanner_process_stream(tlsio->sim808_response_scanner, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response scanner failed to process bytes\n");
			} else if ( xio_stream_scanner_process_stream(tlsio->sim808_response_scanner_alternate, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: Alternate TA response scanner failed to process bytes\n");
			} else {
				// Do any additional stream processing required for this state
			}
			break;
		  case TLSIO_OPEN:
			// Process bytes to send to overlaying XIO layer
			if ( xio_stream_filter_process_stream(tlsio->sim808_response_tcp_traffic_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TCP traffic filter failed to process bytes\n");
			} else if ( NULL == (tcp_traffic_buffer = xio_stream_filter_contents(tlsio->sim808_response_tcp_traffic_filter, &tcp_traffic_buffer_size))
			  && (0 != tcp_traffic_buffer_size)
			) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TCP traffic filter buffer overflow\n");
			} else if ( ('F' == *tcp_traffic_buffer)
			  && xio_stream_filter_full(tlsio->sim808_response_tcp_traffic_filter)
			) {
				tlsio->tlsio_clear_to_send = true;
				if ( tlsio->on_send_complete ) { tlsio->on_send_complete(tlsio->on_send_complete_context, IO_SEND_ERROR); }
			} else if ( 0 != tcp_traffic_buffer_size ) {
				LogInfo("TLSIO sending TCP traffic to caller");
				if ( tlsio->on_bytes_received ) { tlsio->on_bytes_received(tlsio->on_bytes_received_context, tcp_traffic_buffer, tcp_traffic_buffer_size); }
			} else if ( xio_stream_filter_full(tlsio->sim808_response_tcp_traffic_filter) ) {
				tlsio->tlsio_clear_to_send = true;
				if ( tlsio->on_send_complete ) { tlsio->on_send_complete(tlsio->on_send_complete_context, IO_SEND_OK); }
			} else {
				// Do any additional stream processing required for this state
			}
			break;
		  case TLSIO_CLOSE_REQUESTED:
			if ( xio_stream_filter_process_stream(tlsio->sim808_ta_response_code_filter, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response code filter failed to process bytes\n");
			} else if ( xio_stream_scanner_process_stream(tlsio->sim808_response_scanner, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: TA response scanner failed to process bytes\n");
			} else if ( xio_stream_scanner_process_stream(tlsio->sim808_response_scanner_alternate, buffer_, size_) ) {
				if ( tlsio->on_io_error ) { tlsio->on_io_error(tlsio->on_io_error_context); }
				LogError("TLSIO: Alternate TA response scanner failed to process bytes\n");
			} else {
				// Do any additional stream processing required for this state
			}
			break;
		  case TLSIO_OPEN_REQUESTED:
		  case TLSIO_CLOSED:
			break;
		  default:
			LogError("Attempted to process unknown state <%i>\n", tlsio->tlsio_state);
			break;
		}
	}

	return;
}


/******************************************************************************
 * OptionHandler Interface
 ******************************************************************************/


void *
tlsio_sim808_cloneoption (
	const char * option_name_,
	const void * option_value_
) {
	void * clone = NULL;

	if ( !option_name_ ) {
		LogError("Option name was not supplied to `tlsio_sim808_cloneoption()`\n");
	} else if ( !option_value_ ) {
		LogError("Option value was not supplied to `tlsio_sim808_cloneoption()`\n");
	} else if ( !strncmp("apn", option_name_, 3) ) {
		size_t apn_length = strlen((const char *)option_value_);
		if ( apn_length > APN_LENGTH_MAX ) { apn_length = APN_LENGTH_MAX; }
		if ( NULL == (clone = malloc(apn_length + 1)) ) {
			LogError("Failed to clone tlsio_sim808 option \"apn\"\n");
		} else {
			(void)strncpy((char *)clone, (const char *)option_value_, apn_length);
			((char *)clone)[apn_length] = '\0';
			LogInfo("tlsio_sim808 option \"apn\" cloned\n");
		}
	} else if ( !strncmp("hostname", option_name_, 8) ) {
		if ( NULL == (clone = malloc(strlen((const char *)option_value_) + 1)) ) {
			LogError("Failed to clone tlsio_sim808 option \"hostname\"\n");
		} else {
			(void)strcpy((char *)clone, (const char *)option_value_);
			LogInfo("tlsio_sim808 option \"hostname\" cloned\n");
		}
	} else if ( !strncmp("port", option_name_, 4) ) {
		if ( NULL == (clone = malloc(sizeof(int))) ) {
			LogError("Failed to clone tlsio_sim808 option \"port\"\n");
		} else {
			*((int *)clone) = *((int *)option_value_);
			LogInfo("tlsio_sim808 option \"port\" cloned\n");
		}
	} else {
		LogError("Failed to clone unrecognized tlsio_sim808 option \"%s\"\n", option_name_);
		clone = NULL;
	}

	return clone;
}


void
tlsio_sim808_destroyoption (
	const char * option_name_,
	const void * option_value_
) {
	if ( !option_name_ ) { return; }
	if ( !option_value_ ) { return; }

	if ( !strncmp("apn", option_name_, 3)
	  || !strncmp("hostname", option_name_, 8)
	  || !strncmp("port", option_name_, 4)
	) {
		(void)free((void *)option_value_);
		LogInfo("tlsio_sim808 option \"%s\" destroyed\n", option_name_);
	} else {
		LogError("Failed to destroy unrecognized tlsio_sim808 option \"%s\"\n", option_name_);
	}

	return;
}


/******************************************************************************
 * XIO Interface
 ******************************************************************************/


int
tlsio_sim808_close (
	CONCRETE_IO_HANDLE tlsio_sim808_,
	ON_IO_CLOSE_COMPLETE on_io_close_complete_,
	void * callback_context_
) {
	const uint8_t COMMAND_SIGNAL = 0x1B;

	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;
	int error = 0;

	if ( !_singleton || (_singleton != tlsio_sim808_) ) {
		error = __LINE__;
	} else {
		tlsio->on_io_close_complete = on_io_close_complete_;
		tlsio->on_io_close_complete_context = callback_context_;

		switch (tlsio->tlsio_state) {
		  case TLSIO_CLOSED:
			if ( on_io_close_complete_ ) { on_io_close_complete_(callback_context_); }
			break;
		  case TLSIO_CLOSE_REQUESTED:
			break;
		  case TLSIO_OPEN:
			// Send '0x1B' (ESC) to enter command mode and shutdown SIM808
			if ( xio_send(tlsio->sub_io, &COMMAND_SIGNAL, 1, NULL, NULL) ) {
				error = __LINE__;
			} else {
				tlsio->tlsio_state = TLSIO_CLOSE_REQUESTED;
			}
			break;
		  case TLSIO_OPEN_REQUESTED:
			tlsio->tlsio_state = TLSIO_CLOSE_REQUESTED;
			if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_CANCELLED); }
			break;
		  case TLSIO_OPEN_REQUESTED_ATTACH_TO_NETWORK:
			tlsio->tlsio_state = TLSIO_CLOSED;
			if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_CANCELLED); }
			break;
		  case TLSIO_OPEN_REQUESTED_OPEN_TLS_CHANNEL:
			tlsio->tlsio_state = TLSIO_CLOSE_REQUESTED;
			if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_CANCELLED); }
			break;
		  case TLSIO_OPEN_REQUESTED_SYSTEM_CHECK:
			tlsio->tlsio_state = TLSIO_CLOSED;
			if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_CANCELLED); }
			break;
		}
	}

	return error;
}


CONCRETE_IO_HANDLE
tlsio_sim808_create (
	void * io_create_parameters_
) {
	//TODO: Instantiate and destroy filters only during states they are used in order to free resources
	TLSIO_CONFIG * tls_params = (TLSIO_CONFIG *)io_create_parameters_;
	TlsIoContext * tlsio = NULL;

	if ( _singleton ) {
		LogError("Cannot have multiple instances of hardware\n");
		return _singleton;
	} else if ( !io_create_parameters_ ) {
		LogError("Create parameters are mandatory and were not provided\n");
		return NULL;
	} else if ( NULL == (tlsio = (TlsIoContext *)malloc(sizeof(TlsIoContext))) ) {
		LogError("Not enough memory available to create the tlsio_sim808 state\n");
	} else if ( NULL == (tlsio->sub_io = xio_create(uartio_get_interface_description(), (const void *)(&(UARTIO_CONFIG){.baud_rate = 9600, .ring_buffer_size = 4}))) ) {
		LogError("Not enough memory available to create the sub_io state\n");
		(void)free(tlsio);
	} else if ( NULL == (tlsio->tick_counter = tickcounter_create()) ) {
		LogError("Failed to create the tick counter\n");
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_ta_response_code_filter = xio_stream_filter_create(xio_stream_filter_get_ta_response_code_interface())) ) {
		LogError("Failed to create the ta response code stream filter\n");
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_response_ip_address_filter = xio_stream_filter_create(xio_stream_filter_get_ip_address_interface())) ) {
		LogError("Failed to create the ip address stream filter\n");
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_response_registration_query_filter = xio_stream_filter_create(xio_stream_filter_get_registration_query_response_interface())) ) {
		LogError("Failed to create the registration query response stream filter\n");
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_response_tcp_data_max_size_filter = xio_stream_filter_create(xio_stream_filter_get_tcp_data_max_size_interface())) ) {
		LogError("Failed to create the tcp data max size stream filter\n");
		(void)xio_stream_filter_destroy(tlsio->sim808_response_registration_query_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_response_tcp_traffic_filter = xio_stream_filter_create(xio_stream_filter_get_tcp_traffic_interface())) ) {
		LogError("Failed to create the tcp traffic stream filter\n");
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_data_max_size_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_registration_query_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_response_scanner = xio_stream_scanner_create()) ) {
		LogError("Failed to create the default stream scanner\n");
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_traffic_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_data_max_size_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_registration_query_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( NULL == (tlsio->sim808_response_scanner_alternate = xio_stream_scanner_create()) ) {
		LogError("Failed to create the alternate stream scanner\n");
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_traffic_filter);
		(void)xio_stream_scanner_destroy(tlsio->sim808_response_scanner);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_data_max_size_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_registration_query_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else if ( (NULL == (tlsio->tlsio_config.hostname = tlsio_sim808_cloneoption("hostname", (void *)tls_params->hostname))) ) {
		LogError("Not enough memory available to copy in the target hostname\n");
		(void)xio_stream_scanner_destroy(tlsio->sim808_response_scanner_alternate);
		(void)xio_stream_scanner_destroy(tlsio->sim808_response_scanner);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_data_max_size_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_registration_query_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);
		(void)free(tlsio);
	} else {
		// Initialize the remaining state variables
		tlsio->on_bytes_received = NULL;
		tlsio->on_bytes_received_context = NULL;
		tlsio->on_io_error = NULL;
		tlsio->on_io_error_context = NULL;
		tlsio->on_io_open_complete = NULL;
		tlsio->on_io_open_complete_context = NULL;
		tlsio->sim808_access_point_name = NULL;
		tlsio->sim808_at_command_fail_time_ms = 0;
		tlsio->sim808_at_command_send_time_ms = 0;
		tlsio->sim808_tcp_data_max_size = 0;
		tlsio->state_machine_index = 0;
		tlsio->sub_io_open = false;
		tlsio->tlsio_clear_to_send = true;
		tlsio->tlsio_config.port = tls_params->port;
		tlsio->tlsio_state = TLSIO_CLOSED;

		// The tlsio layer models the interaction with a single physical device,
		// the TLS module of the SIM808, and does not support multiple instances
		_singleton = (XIO_HANDLE)tlsio;
		LogInfo("tlsio has been successfully created\n");
	}

	return _singleton;
}


void
tlsio_sim808_destroy (
	CONCRETE_IO_HANDLE tlsio_sim808_
) {
	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;

	if ( !_singleton || (_singleton != tlsio_sim808_) ) {
		LogError("Invalid context provided\n");
	} else {
		// Ensure graceful shutdown
		if ( TLSIO_OPEN == tlsio->tlsio_state ) {
			(void)tlsio_sim808_close(tlsio_sim808_, NULL, NULL);
			for(; TLSIO_CLOSED != tlsio->tlsio_state ;) {
				(void)tlsio_sim808_dowork(tlsio_sim808_);
			}
		}

		// Must be deallocated here in case xio to prevent race
		// condition when close is called before open completes
		tlsio->on_io_open_complete = NULL;
		tlsio->on_io_open_complete_context = NULL;

		// Deallocate user defined values
		(void)tlsio_sim808_destroyoption("apn", tlsio->sim808_access_point_name);
		(void)tlsio_sim808_destroyoption("hostname", tlsio->tlsio_config.hostname);

		// Deallocate the sub_io layer and context(s)
		(void)xio_stream_scanner_destroy(tlsio->sim808_response_scanner_alternate);
		(void)xio_stream_scanner_destroy(tlsio->sim808_response_scanner);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_tcp_data_max_size_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_registration_query_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_response_ip_address_filter);
		(void)xio_stream_filter_destroy(tlsio->sim808_ta_response_code_filter);
		(void)tickcounter_destroy(tlsio->tick_counter);
		(void)xio_destroy(tlsio->sub_io);

		// Deallocate the singleton pointer
		(void)free(tlsio_sim808_);
		_singleton = NULL;
	}
}


void
tlsio_sim808_dowork (
	CONCRETE_IO_HANDLE tlsio_sim808_
) {
	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;

	if ( !_singleton
	  || (_singleton != tlsio_sim808_)
	) {
		LogError("Caller failed to pass context!\n");
	} else {
		// Process the underlying xio
		(void)xio_dowork(tlsio->sub_io);

		switch (tlsio->tlsio_state) {
		  case TLSIO_OPEN_REQUESTED:
			tlsio->tlsio_state = (TlsIoState)(
				(tlsio->sub_io_open * TLSIO_OPEN_REQUESTED_SYSTEM_CHECK) +
				(!tlsio->sub_io_open * TLSIO_OPEN_REQUESTED)
			);
			break;
		  case TLSIO_OPEN_REQUESTED_SYSTEM_CHECK:
		    if ( tlsioOpenRequested_systemCheck(tlsio_sim808_) ) {
				LogError("Unable to complete sim808 system check\n");
		    	if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_ERROR); }
		    } else {
		    	// Do other processing required for this state
		    }
			break;
		  case TLSIO_OPEN_REQUESTED_ATTACH_TO_NETWORK:
			if ( tlsioOpenRequested_attachToNetwork(tlsio_sim808_) ) {
				LogError("Unable to attach sim808 to GPRS network\n");
		    	if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_ERROR); }
			} else {
				// Do other processing required for this state
			}
			break;
		  case TLSIO_OPEN_REQUESTED_OPEN_TLS_CHANNEL:
			if ( tlsioOpenRequested_openTlsChannel(tlsio_sim808_) ) {
				LogError("Unable to open TLS channel via sim808\n");
		    	if ( tlsio->on_io_open_complete ) { tlsio->on_io_open_complete(tlsio->on_io_open_complete_context, IO_OPEN_ERROR); }
			} else {
				// Do other processing required for this state
			}
			break;
		  case TLSIO_CLOSE_REQUESTED:
			if ( tlsioCloseRequested(tlsio_sim808_) ) {
				LogError("Unable to close sim808 without error\n");
			} else {
				// Do other processing required for this state
			}
			break;
		  case TLSIO_OPEN:
			//TODO: Confirm connection stays open after server send and response
		  case TLSIO_CLOSED:
			break;
		  default:
			LogError("Attempted to process unknown state <%i>\n", tlsio->tlsio_state);
			break;
		}
	}
	return;
}


const IO_INTERFACE_DESCRIPTION *
tlsio_sim808_get_interface_description (
	void
) {
	return &_tlsio_sim808_interface_description;
}


int
tlsio_sim808_open (
	CONCRETE_IO_HANDLE tlsio_sim808_,
	ON_IO_OPEN_COMPLETE on_io_open_complete_,
	void * on_io_open_complete_context_,
	ON_BYTES_RECEIVED on_bytes_received_,
	void * on_bytes_received_context_,
	ON_IO_ERROR on_io_error_,
	void * on_io_error_context_
) {
	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;
	int error = 0;

	if ( !_singleton || (_singleton != tlsio_sim808_) ) {
		error = __LINE__;
		if ( on_io_open_complete_ ) { on_io_open_complete_(on_io_open_complete_context_, IO_OPEN_ERROR); }
	} else if ( !tlsio->sim808_access_point_name ) {
		error = __LINE__;
		if ( on_io_open_complete_ ) { on_io_open_complete_(on_io_open_complete_context_, IO_OPEN_ERROR); }
	} else if ( TLSIO_CLOSED != tlsio->tlsio_state ) {
		error = __LINE__;
		if ( on_io_open_complete_ ) { on_io_open_complete_(on_io_open_complete_context_, IO_OPEN_ERROR); }
	} else {
		// Save callback details
		tlsio->on_bytes_received = on_bytes_received_;
		tlsio->on_bytes_received_context = on_bytes_received_context_;
		tlsio->on_io_error = on_io_error_;
		tlsio->on_io_error_context = on_io_error_context_;
		tlsio->on_io_open_complete = on_io_open_complete_;
		tlsio->on_io_open_complete_context = on_io_open_complete_context_;

		// Open underlying xio
		//TODO: Figure out better error handling scheme for underlying xio layer
		if ( xio_open(tlsio->sub_io, onUnderlyingIoOpenComplete, tlsio_sim808_, processUnderlyingIo, tlsio_sim808_, on_io_error_, on_io_error_context_) ) {
			error = __LINE__;
		// (Re)Initialize variables
		} else if (xio_stream_filter_reset(tlsio->sim808_response_ip_address_filter) ) {
			error = __LINE__;
		} else if (xio_stream_filter_reset(tlsio->sim808_response_registration_query_filter) ) {
			error = __LINE__;
		} else if ( xio_stream_filter_reset(tlsio->sim808_ta_response_code_filter) ) {
			error = __LINE__;
		} else if ( xio_stream_filter_reset(tlsio->sim808_response_tcp_data_max_size_filter) ) {
			error = __LINE__;
		} else if ( xio_stream_filter_reset(tlsio->sim808_response_tcp_traffic_filter) ) {
			error = __LINE__;
		} else if (xio_stream_scanner_init(tlsio->sim808_response_scanner, "SIMCOM_SIM808\r\n", (sizeof("SIMCOM_SIM808\r\n") - 1)) ) {
			error = __LINE__;
		} else if (xio_stream_scanner_init(tlsio->sim808_response_scanner_alternate, "\r\nCONNECT FAIL\r\n", (sizeof("\r\nCONNECT FAIL\r\n") - 1)) ) {
			error = __LINE__;
		} else {
			LogInfo("SIM808 initialization has been initiated\n");
			tlsio->sim808_at_command_fail_time_ms = 0;
			tlsio->sim808_at_command_send_time_ms = 0;
			tlsio->sim808_tcp_data_max_size = 0;
			tlsio->state_machine_index = 0;
			tlsio->tlsio_clear_to_send = true;
			tlsio->tlsio_state = TLSIO_OPEN_REQUESTED;
		}
	}

	return error;
}


OPTIONHANDLER_HANDLE
tlsio_sim808_retrieveoptions (
	CONCRETE_IO_HANDLE tlsio_sim808_
) {
	if ( !_singleton || (_singleton != tlsio_sim808_) ) { return NULL; }

	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;
	OPTIONHANDLER_HANDLE options = NULL;

	if ( NULL == (options = OptionHandler_Create(tlsio_sim808_cloneoption, tlsio_sim808_destroyoption, tlsio_sim808_setoption)) ) {
		//(void)LogError("Unable to allocate memory for tlsio_sim808 options\n");
	} else if ( OptionHandler_AddOption(options, "apn", (void *)tlsio->sim808_access_point_name) ) {
		OptionHandler_Destroy(options);
		options = NULL;
		//(void)LogError("Failed to retrieve tlsio_sim808 option \"apn\"\n");
	} else if ( OptionHandler_AddOption(options, "hostname", (void *)tlsio->tlsio_config.hostname) ) {
		OptionHandler_Destroy(options);
		options = NULL;
		//(void)LogError("Failed to retrieve tlsio_sim808 option \"hostname\"\n");
	} else if ( OptionHandler_AddOption(options, "port", (void *)&tlsio->tlsio_config.port) ) {
		OptionHandler_Destroy(options);
		options = NULL;
		//(void)LogError("Failed to retrieve tlsio_sim808 option \"port\"\n");
	} else if ( !tlsio->sub_io ) {
		OptionHandler_Destroy(options);
		options = NULL;
		//(void)LogError("Underlying XIO is unavailable for option retrieval\n");
	} else if ( OptionHandler_AddOption(options, "sub_io_options", xio_retrieveoptions(tlsio->sub_io)) ) {
		OptionHandler_Destroy(options);
		options = NULL;
		//(void)LogError("Failed to retrieve sub_io options\n");
	} else {
		//(void)LogError("Successfully retrieved all tlsio_sim808 options\n");
	}

	return options;
}


int
tlsio_sim808_send (
	CONCRETE_IO_HANDLE tlsio_sim808_,
	const void * buffer_,
	size_t size_,
	ON_SEND_COMPLETE on_send_complete_,
	void * callback_context_
) {
	const uint8_t TERMINATION_SYMBOL = 0x1A;

	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;
	int error = 0;

	if ( !_singleton || (_singleton != tlsio_sim808_) ) {
		error = __LINE__;
	} else if ( !buffer_ ) {
		error = __LINE__;
	} else if ( !tlsio->tlsio_clear_to_send ) {
		error = __LINE__;
	} else if ( TLSIO_OPEN != tlsio->tlsio_state ) {
		error = __LINE__;
	} else {
		//TODO: Test against tlsio->sim808_tcp_data_max_size and break up if necessary
		if ( xio_stream_filter_reset(tlsio->sim808_response_tcp_traffic_filter) ) {
			error = __LINE__;
		} else {
			tlsio->on_send_complete = on_send_complete_;
			tlsio->on_send_complete_context = callback_context_;
			tlsio->tlsio_clear_to_send = false;

			if ( xio_send(tlsio->sub_io, buffer_, size_, NULL, NULL) ) {
				error = __LINE__;
			// Terminate data blobs with '0x1A' (Ctrl+Z) to send
			} else if ( xio_send(tlsio->sub_io, &TERMINATION_SYMBOL, 1, NULL, NULL) ) {
				error = __LINE__;
			}

			//TODO: Block until TLS is clear to send again
			//for (; !tlsio->tlsio_clear_to_send ;) {
			//	(void)xio_dowork(tlsio_sim808_);
			//}
		}
	}

	return error;
}


int
tlsio_sim808_setoption (
	CONCRETE_IO_HANDLE tlsio_sim808_,
	const char * option_name_,
	const void * option_value_
) {
	if ( !_singleton || (_singleton != tlsio_sim808_) ) { return __LINE__; }
	if ( !option_name_ ) { return __LINE__; }
	if ( !option_value_ ) { return __LINE__; }

	TlsIoContext * tlsio = (TlsIoContext *)tlsio_sim808_;
	int error = 0;

	if ( !strncmp("apn", option_name_, 3) ) {
		(void)tlsio_sim808_destroyoption("apn", (void *)tlsio->sim808_access_point_name);
		if ( NULL == (tlsio->sim808_access_point_name = tlsio_sim808_cloneoption(option_name_, option_value_)) ) {
			error = __LINE__;
			//LogError("Failed to set tlsio_sim808 option \"apn\"\n");
		} else {
			//LogError("tlsio_sim808 option \"apn\" set\n");
		}
	} else if ( !strncmp("hostname", option_name_, 8) ) {
		(void)tlsio_sim808_destroyoption("hostname", (void *)tlsio->tlsio_config.hostname);
		if ( NULL == (tlsio->tlsio_config.hostname = tlsio_sim808_cloneoption(option_name_, option_value_)) ) {
			error = __LINE__;
			//LogError("Failed to set tlsio_sim808 option \"hostname\"\n");
		} else {
			//LogError("tlsio_sim808 option \"hostname\" set\n");
		}
	} else if ( !strncmp("port", option_name_, 4) ) {
		tlsio->tlsio_config.port = *((int *)option_value_);
		//LogError("tlsio_sim808 option \"port\" set\n");
	} else if ( !strncmp("sub_io_options", option_name_, 14) ) {
		//LogError("Feeding options to underlying xio layer...\n");
		if ( OptionHandler_FeedOptions((OPTIONHANDLER_HANDLE)option_value_, tlsio->sub_io) ) {
			//LogError("Unable to apply sub_io options\n");
			error = __LINE__;
		} else {
			//LogError("option \"%s\" successfully passed to lower layer\n", option_name_);
			(void)OptionHandler_Destroy((OPTIONHANDLER_HANDLE)option_value_);
		}
	} else if ( xio_setoption(tlsio->sub_io, option_name_, option_value_) ) {
		//LogError("Unable to apply option \"%s\" to lower layer\n", option_name_);
		error = __LINE__;
	} else {
		//LogError("option \"%s\" successfully passed to lower layer\n", option_name_);
	}

	return error;
}


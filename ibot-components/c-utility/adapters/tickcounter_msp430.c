// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <stdlib.h>
#ifdef _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

#include <driverlib.h>

#include "azure_c_shared_utility/tickcounter.h"
#include "azure_c_shared_utility/core_msp430.h"

typedef struct TickCounterState {
	tick_t creation_offset;
} TickCounterState;


TICK_COUNTER_HANDLE
tickcounter_create (
	void
) {
	TickCounterState * tick_counter = NULL;

	if ( NULL == (tick_counter = (TickCounterState *)malloc(sizeof(TickCounterState))) ) {
		//LogError("Unable to allocate tick counter state\n");
	} else {
		tick_counter->creation_offset = timer_a3_ticks();
	}

	return (TICK_COUNTER_HANDLE)tick_counter;
}


void
tickcounter_destroy (
	TICK_COUNTER_HANDLE context_
) {
	(void)free(context_);
}


int
tickcounter_get_current_ms (
	TICK_COUNTER_HANDLE context_,
	tickcounter_ms_t * current_ms_
) {
	TickCounterState * tick_counter = (TickCounterState *)context_;
	int error = 0;

	if ( !context_ ) {
		error = __LINE__;
	} else if ( !current_ms_ ) {
		// Is this really an error?
	} else {
	  #pragma diag_push
	  /*
	   * (ULP 5.1) Detected divide operation(s).
	   *
	   * Recommend moving them to RAM during run time or
	   * not using as these are processing/power intensive
	   */
	  //TODO: Confirm decision or address suppression diagnostic message
	  #pragma diag_suppress 1530
		*current_ms_ = (tickcounter_ms_t)(((timer_a3_ticks() - tick_counter->creation_offset) << 10) / TIMER_A3_TICKS_PER_SEC);
	  #pragma diag_pop
	}

	return error;
}


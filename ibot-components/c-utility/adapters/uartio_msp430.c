// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <driverlib.h>

#include "azure_c_shared_utility/uartio.h"

#define SIM808_RX BIT5
#define SIM808_TX BIT6

typedef struct UartIoState {
	uint8_t * eusci_a1_cache_buffer;
	uint8_t * eusci_a1_ring_buffer;
	size_t eusci_a1_ring_buffer_head;
	size_t eusci_a1_ring_buffer_index_mask;
	size_t eusci_a1_ring_buffer_tail;
	uint8_t eusci_a1_rx_error;
	bool eusci_a1_ring_buffer_overflow;
	bool eusci_a1_ring_buffer_full;
	ON_IO_OPEN_COMPLETE on_io_open_complete;
	void * on_io_open_complete_context;
	ON_BYTES_RECEIVED on_bytes_received;
	void * on_bytes_received_context;
	ON_IO_ERROR on_io_error;
	void * on_io_error_context;
	UARTIO_CONFIG uart_config;
	bool uartio_open;
} UartIoState;

void
initializeEusciAParametersForSMClkAtBaudRate (
	EUSCI_A_UART_initParam * eusci_a_parameters_,
	uint32_t baud_rate_
);

uint8_t
secondModulationRegisterValueFromFractionalPortion (
	float fractional_portion_
);

void *
uartio_cloneoption (
	const char * option_name_,
	const void * option_value_
);

int
uartio_close (
	CONCRETE_IO_HANDLE uartio_,
	ON_IO_CLOSE_COMPLETE on_io_close_complete_,
	void * callback_context_
);

CONCRETE_IO_HANDLE
uartio_create (
	void * io_create_parameters_
);

void
uartio_destroy (
	CONCRETE_IO_HANDLE uartio_
);

void
uartio_destroyoption (
	const char * option_name_,
	const void * option_value_
);

void
uartio_dowork (
	CONCRETE_IO_HANDLE uartio_
);

int
uartio_open (
	CONCRETE_IO_HANDLE uartio_,
	ON_IO_OPEN_COMPLETE on_io_open_complete_,
	void * on_io_open_complete_context_,
	ON_BYTES_RECEIVED on_bytes_received_,
	void * on_bytes_received_context_,
	ON_IO_ERROR on_io_error_,
	void * on_io_error_context_
);

OPTIONHANDLER_HANDLE
uartio_retrieveoptions (
	CONCRETE_IO_HANDLE uartio_
);

int
uartio_send (
	CONCRETE_IO_HANDLE uartio_,
	const void * const buffer_,
	const size_t buffer_size_,
	ON_SEND_COMPLETE on_send_complete_,
	void * callback_context_
);

int
uartio_setoption (
	CONCRETE_IO_HANDLE uartio_,
	const char * const option_name_,
	const void * const option_value_
);

#pragma diag_push
/*
 * (ULP 7.1) Detected use of global variable "_uartio_interface_description"
 * within one function "uartio_get_interface_description".
 *
 * Recommend placing variable in the function locally
 */
//TODO: Confirm decision or address suppression diagnostic message
#pragma diag_suppress 1534
static const IO_INTERFACE_DESCRIPTION _uartio_interface_description = {
	.concrete_io_close = uartio_close,
	.concrete_io_create = uartio_create,
	.concrete_io_destroy = uartio_destroy,
	.concrete_io_dowork = uartio_dowork,
	.concrete_io_open = uartio_open,
	.concrete_io_retrieveoptions = uartio_retrieveoptions,
	.concrete_io_send = uartio_send,
	.concrete_io_setoption = uartio_setoption
};
#pragma diag_pop

static XIO_HANDLE _singleton = NULL;


/******************************************************************************
 * Interrupt to signal SIM808 UART RX data is available
 ******************************************************************************/
#pragma vector = USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
{
	switch (__even_in_range(UCA1IV,8))
	{
      case 0x00: break;
      case 0x02:
       #pragma diag_push
       /*
        * (ULP 10.1) ISR USCI_A1_ISR calls function EUSCI_A_UART_queryStatusFlags.
        *
        * Recommend moving function call away from ISR, or inlining the function, or using pragmas
        */
       //TODO: Confirm decision or address suppression diagnostic message
       #pragma diag_suppress 1538
    	((UartIoState *)_singleton)->eusci_a1_rx_error = EUSCI_A_UART_queryStatusFlags(EUSCI_A1_BASE, (UCFE | UCPE | UCOE));
    	((UartIoState *)_singleton)->eusci_a1_ring_buffer[((UartIoState *)_singleton)->eusci_a1_ring_buffer_head] = EUSCI_A_UART_receiveData(EUSCI_A1_BASE);
       #pragma diag_pop
    	((UartIoState *)_singleton)->eusci_a1_ring_buffer_overflow = ((UartIoState *)_singleton)->eusci_a1_ring_buffer_full;
    	((UartIoState *)_singleton)->eusci_a1_ring_buffer_head = (((UartIoState *)_singleton)->eusci_a1_ring_buffer_index_mask & (((UartIoState *)_singleton)->eusci_a1_ring_buffer_head + 1));
    	((UartIoState *)_singleton)->eusci_a1_ring_buffer_full = ((((UartIoState *)_singleton)->eusci_a1_ring_buffer_head == ((UartIoState *)_singleton)->eusci_a1_ring_buffer_tail) || ((UartIoState *)_singleton)->eusci_a1_ring_buffer_overflow);
		break;
      case 0x04: break;
      case 0x06: break;
      case 0x08: break;
      default: __never_executed();
	}
}


/******************************************************************************
 * Initialize EUSCI_A parameters required to communicate with the SIM808
 *
 * NOTE: MSP430FR5969 User's Guide 24.3.10
 ******************************************************************************/
void
initializeEusciAParametersForSMClkAtBaudRate (
	EUSCI_A_UART_initParam * eusci_a_parameters_,
	uint32_t baud_rate_
) {
	float datasheet_N = 0.0f;
	float datasheet_N_oversampled = 0.0f;
	uint16_t register_ucaxbrw = 0x0000;
	uint8_t register_ucbrf = 0x00;
	uint8_t register_ucbrs = 0x00;
	uint16_t register_ucos16 = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;

	// Algorithm from User's Guide (Section 24.3.10)
  #pragma diag_push
  /*
   * (ULP 5.1) Detected divide operation(s).
   *
   * Recommend moving them to RAM during run time or not using as these are processing/power intensive
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1530
  /*
   * (ULP 5.2) Detected floating point operation(s).
   *
   * Recommend moving them to RAM during run time or not using as these are processing/power intensive
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1531
	datasheet_N = (CS_getSMCLK() / (float)baud_rate_);

	if ( datasheet_N >= 16 ) {
		register_ucos16 = EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION;
		datasheet_N_oversampled = datasheet_N / 16;
		register_ucaxbrw = (uint16_t)datasheet_N_oversampled;
		register_ucbrf = (uint8_t)((datasheet_N_oversampled - register_ucaxbrw) * 16);
	} else {
		register_ucos16 = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;
		register_ucaxbrw = (uint16_t)datasheet_N;
		register_ucbrf = 0x00;
	}
	register_ucbrs = secondModulationRegisterValueFromFractionalPortion(datasheet_N - (uint16_t)datasheet_N);
  #pragma diag_pop

	eusci_a_parameters_->selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
	eusci_a_parameters_->clockPrescalar = register_ucaxbrw;
	eusci_a_parameters_->firstModReg = register_ucbrf;
	eusci_a_parameters_->secondModReg = register_ucbrs;
	eusci_a_parameters_->parity = EUSCI_A_UART_NO_PARITY;
	eusci_a_parameters_->msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
	eusci_a_parameters_->numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
	eusci_a_parameters_->uartMode = EUSCI_A_UART_MODE;
	eusci_a_parameters_->overSampling = register_ucos16;
}


/******************************************************************************
 * Calculate the secondary modulation register value
 *
 * NOTE: Table 24-4 MSP430FR5969 User's Guide 24.3.10
 ******************************************************************************/
#pragma diag_push
/*
 * (ULP 5.2) Detected floating point operation(s).
 *
 * Recommend moving them to RAM during run time or not using as these are processing/power intensive
 */
//TODO: Confirm decision or address suppression diagnostic message
#pragma diag_suppress 1531
uint8_t
secondModulationRegisterValueFromFractionalPortion (
	float fractional_portion_
) {
	uint8_t ucbrsx = 0x00;

	if ( fractional_portion_ >= 0.9288f ) {
		ucbrsx = 0xFE;  // 11111110
	} else if ( fractional_portion_ >= 0.9170f ) {
		ucbrsx = 0xFD;  // 11111101
	} else if ( fractional_portion_ >= 0.9004f ) {
		ucbrsx = 0xFB;  // 11111011
	} else if ( fractional_portion_ >= 0.8751f ) {
		ucbrsx = 0xF7;  // 11110111
	} else if ( fractional_portion_ >= 0.8572f ) {
		ucbrsx = 0xEF;  // 11101111
	} else if ( fractional_portion_ >= 0.8464f ) {
		ucbrsx = 0xDF;  // 11011111
	} else if ( fractional_portion_ >= 0.8333f ) {
		ucbrsx = 0xBF;  // 10111111
	} else if ( fractional_portion_ >= 0.8004f ) {
		ucbrsx = 0xEE;  // 11101110
	} else if ( fractional_portion_ >= 0.7861f ) {
		ucbrsx = 0xED;  // 11101101
	} else if ( fractional_portion_ >= 0.7503f ) {
		ucbrsx = 0xDD;  // 11011101
	} else if ( fractional_portion_ >= 0.7147f ) {
		ucbrsx = 0xBB;  // 10111011
	} else if ( fractional_portion_ >= 0.7001f ) {
		ucbrsx = 0xB7;  // 10110111
	} else if ( fractional_portion_ >= 0.6667f ) {
		ucbrsx = 0xD6;  // 11010110
	} else if ( fractional_portion_ >= 0.6432f ) {
		ucbrsx = 0xB6;  // 10110110
	} else if ( fractional_portion_ >= 0.6254f ) {
		ucbrsx = 0xB5;  // 10110101
	} else if ( fractional_portion_ >= 0.6003f ) {
		ucbrsx = 0xAD;  // 10101101
	} else if ( fractional_portion_ >= 0.5715f ) {
		ucbrsx = 0x6B;  // 01101011
	} else if ( fractional_portion_ >= 0.5002f ) {
		ucbrsx = 0xAA;  // 10101010
	} else if ( fractional_portion_ >= 0.4378f ) {
		ucbrsx = 0x55;  // 01010101
	} else if ( fractional_portion_ >= 0.4286f ) {
		ucbrsx = 0x53;  // 01010011
	} else if ( fractional_portion_ >= 0.4003f ) {
		ucbrsx = 0x92;  // 10010010
	} else if ( fractional_portion_ >= 0.3753f ) {
		ucbrsx = 0x52;  // 01010010
	} else if ( fractional_portion_ >= 0.3575f ) {
		ucbrsx = 0x4A;  // 01001010
	} else if ( fractional_portion_ >= 0.3335f ) {
		ucbrsx = 0x49;  // 01001001
	} else if ( fractional_portion_ >= 0.3000f ) {
		ucbrsx = 0x25;  // 00100101
	} else if ( fractional_portion_ >= 0.2503f ) {
		ucbrsx = 0x44;  // 01000100
	} else if ( fractional_portion_ >= 0.2224f ) {
		ucbrsx = 0x22;  // 00100010
	} else if ( fractional_portion_ >= 0.2147f ) {
		ucbrsx = 0x21;  // 00100001
	} else if ( fractional_portion_ >= 0.1670f ) {
		ucbrsx = 0x11;  // 00010001
	} else if ( fractional_portion_ >= 0.1430f ) {
		ucbrsx = 0x20;  // 00100000
	} else if ( fractional_portion_ >= 0.1252f ) {
		ucbrsx = 0x10;  // 00010000
	} else if ( fractional_portion_ >= 0.1001f ) {
		ucbrsx = 0x08;  // 00001000
	} else if ( fractional_portion_ >= 0.0835f ) {
		ucbrsx = 0x04;  // 00000100
	} else if ( fractional_portion_ >= 0.0715f ) {
		ucbrsx = 0x02;  // 00000010
	} else if ( fractional_portion_ >= 0.0529f ) {
		ucbrsx = 0x01;  // 00000001
	} else {
		ucbrsx = 0x00;  // 00000000
	}

	return ucbrsx;
}
#pragma diag_pop


int
uartio_close (
	CONCRETE_IO_HANDLE uartio_,
	ON_IO_CLOSE_COMPLETE on_io_close_complete_,
	void * callback_context_
) {
	if ( !_singleton || (_singleton != uartio_) ) { return __LINE__; }

	UartIoState * uart_state = (UartIoState *)uartio_;

	if ( uart_state->uartio_open ) {
  #pragma diag_push
  /*
   * (ULP 2.1) Detected SW delay loop using empty loop.
   *
   * Recommend using a timer module instead
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1527
		for (; EUSCI_A_UART_queryStatusFlags(EUSCI_A1_BASE, UCBUSY) ;);  // Wait for outstanding serial transactions to complete
  #pragma diag_pop

		// Disable UART
		(void)EUSCI_A_UART_disableInterrupt(EUSCI_A1_BASE, UCRXIE);
		(void)EUSCI_A_UART_disable(EUSCI_A1_BASE);

		uart_state->on_bytes_received = NULL;
		uart_state->on_bytes_received_context = NULL;
		uart_state->on_io_error = NULL;
		uart_state->on_io_error_context = NULL;
		uart_state->uartio_open = false;
	}
	if ( on_io_close_complete_ ) { on_io_close_complete_(callback_context_); }
	return 0;
}


CONCRETE_IO_HANDLE
uartio_create (
	void * io_create_parameters_
) {
	UARTIO_CONFIG * uart_params = (UARTIO_CONFIG *)io_create_parameters_;
	UartIoState * uart_state = NULL;

	// Cannot have multiple instances of hardware
	if ( _singleton ) {
		return _singleton;
	} else if ( !io_create_parameters_ ) {
		return NULL;
	// Allocate the state associated to the UART
	} else if ( NULL == (uart_state = (UartIoState *)malloc(sizeof(UartIoState))) ) {
		//LogError("Not enough memory available to create the uartio state"\n");
	// Allocate the ring buffer
	} else {
		size_t pow_2 = 0;
	  #pragma diag_push
	  /*
	   * (ULP 2.1) Detected SW delay loop using empty loop.
	   *
	   * Recommend using a timer module instead
	   */
	  //TODO: Confirm decision or address suppression diagnostic message
	  #pragma diag_suppress 1527
	  /*
	   * (ULP 13.1) Detected loop counting up.
	   *
	   * Recommend loops count down as detecting zeros is easier
	   */
	  //TODO: Confirm decision or address suppression diagnostic message
	  #pragma diag_suppress 1544
		for (; uart_params->ring_buffer_size > (1 << pow_2) ; ++pow_2);
	  #pragma diag_pop
		uart_state->eusci_a1_ring_buffer_index_mask = ((1 << pow_2) - 1);

		if ( NULL == (uart_state->eusci_a1_ring_buffer = (uint8_t *)malloc(uart_state->eusci_a1_ring_buffer_index_mask + 1)) ) {
			(void)free(uart_state);
			//LogError("Not enough memory available to create the ring buffer"\n");
		} else if ( NULL == (uart_state->eusci_a1_cache_buffer = (uint8_t *)malloc(uart_state->eusci_a1_ring_buffer_index_mask + 1)) ) {
			(void)free(uart_state->eusci_a1_ring_buffer);
			(void)free(uart_state);
			//LogError("Not enough memory available to create the cache buffer"\n");
		} else {
			// Initialize the remaining state variables
			uart_state->eusci_a1_ring_buffer_head = 0;
			uart_state->eusci_a1_ring_buffer_tail = 0;
			uart_state->eusci_a1_rx_error = false;
			uart_state->eusci_a1_ring_buffer_overflow = false;
			uart_state->eusci_a1_ring_buffer_full = (1 == uart_params->ring_buffer_size);
			uart_state->uart_config.baud_rate = uart_params->baud_rate;
			uart_state->uart_config.ring_buffer_size = uart_params->ring_buffer_size;
			uart_state->on_bytes_received = NULL;
			uart_state->on_bytes_received_context = NULL;
			uart_state->on_io_error = NULL;
			uart_state->on_io_error_context = NULL;
			uart_state->on_io_open_complete = NULL;
			uart_state->on_io_open_complete_context = NULL;
			uart_state->uartio_open = false;

			// The uartio layer models the interaction with a single physical device,
			// the EUSCI_A1 of the MSP430, and does not support multiple instances
			_singleton = (XIO_HANDLE)uart_state;
			//LogError("uartio has been successfully created\n");
		}
	}

	return _singleton;
}


void
uartio_destroy (
	CONCRETE_IO_HANDLE uartio_
) {
	if ( !_singleton || (_singleton != uartio_) ) { return; }

	UartIoState * uart_state = (UartIoState *)uartio_;

	// Ensure graceful shutdown
	if ( uart_state->uartio_open ) { (void)uartio_close(uartio_, NULL, NULL); }

	// Must be deallocated here in case xio
	// to prevent race condition when close
	// is called before open completes
	uart_state->on_io_open_complete = NULL;
	uart_state->on_io_open_complete_context = NULL;

	// Deallocate the ring and cache buffers
	(void)free(uart_state->eusci_a1_cache_buffer);
	(void)free(uart_state->eusci_a1_ring_buffer);

	// Deallocate the singleton pointer
	(void)free(uartio_);
	_singleton = NULL;
}


void
uartio_dowork (
	CONCRETE_IO_HANDLE uartio_
) {
	static bool error = false;

	UartIoState * uart_state = (UartIoState *)uartio_;

	if ( NULL == _singleton ) {
	} else if (_singleton != uartio_) {
	} else if ( !uart_state->uartio_open ) {
	} else {
		size_t index = 0;

		// CRITICAL SECTION: Read the ring buffer variables
		(void)EUSCI_A_UART_disableInterrupt(EUSCI_A1_BASE, UCRXIE); {  // Lock the receive interrupt flag
			error |= (uart_state->eusci_a1_ring_buffer_overflow || uart_state->eusci_a1_rx_error);
			uart_state->eusci_a1_ring_buffer_overflow = false;

			if ( uart_state->eusci_a1_ring_buffer_full
			  || (uart_state->eusci_a1_ring_buffer_tail != uart_state->eusci_a1_ring_buffer_head)
			) {
				uart_state->eusci_a1_ring_buffer_tail = (
					(uart_state->eusci_a1_ring_buffer_full * uart_state->eusci_a1_ring_buffer_head) +
					(!uart_state->eusci_a1_ring_buffer_full * uart_state->eusci_a1_ring_buffer_tail)
				);  // correct in case of overflow - OVERFLOW BYTES ARE LOST
				do {
					uart_state->eusci_a1_cache_buffer[index] = uart_state->eusci_a1_ring_buffer[uart_state->eusci_a1_ring_buffer_tail];

					uart_state->eusci_a1_ring_buffer_tail = (uart_state->eusci_a1_ring_buffer_index_mask & (uart_state->eusci_a1_ring_buffer_tail + 1));
					++index;
				} while ( uart_state->eusci_a1_ring_buffer_head != uart_state->eusci_a1_ring_buffer_tail );
			}
			uart_state->eusci_a1_ring_buffer_full = false;
		} (void)EUSCI_A_UART_enableInterrupt(EUSCI_A1_BASE, UCRXIE);  // Unlock the receive interrupt flag

		if ( uart_state->on_bytes_received && index ) { uart_state->on_bytes_received(uart_state->on_bytes_received_context, uart_state->eusci_a1_cache_buffer, index); }

		// Check for ring buffer overflow
		if ( uart_state->on_io_error && error ) {
			uart_state->on_io_error(uart_state->on_io_error_context);
			uart_state->eusci_a1_rx_error = 0x00;
		}
	}
}


const IO_INTERFACE_DESCRIPTION *
uartio_get_interface_description (
	void
) {
	return &_uartio_interface_description;
}


int
uartio_open (
	CONCRETE_IO_HANDLE uartio_,
	ON_IO_OPEN_COMPLETE on_io_open_complete_,
	void * on_io_open_complete_context_,
	ON_BYTES_RECEIVED on_bytes_received_,
	void * on_bytes_received_context_,
	ON_IO_ERROR on_io_error_,
	void * on_io_error_context_
) {
	if ( !_singleton || (_singleton != uartio_) ) { return __LINE__; }

	UartIoState * uart_state = (UartIoState *)uartio_;
	EUSCI_A_UART_initParam eusci_a1_parameters = {0};
	int result = 0;

	if ( !uart_state->uartio_open ) {
		// Save callback details
		uart_state->on_io_open_complete = on_io_open_complete_;
		uart_state->on_io_open_complete_context = on_io_open_complete_context_;
		uart_state->on_bytes_received = on_bytes_received_;
		uart_state->on_bytes_received_context = on_bytes_received_context_;
		uart_state->on_io_error = on_io_error_;
		uart_state->on_io_error_context = on_io_error_context_;

		// Initialize EUSCI_A1 Port Pins
		GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P2, (SIM808_RX | SIM808_TX), GPIO_SECONDARY_MODULE_FUNCTION);

		// Initialize UART responsible for communication with SIM808
		uart_state->eusci_a1_ring_buffer_overflow = false;
		uart_state->eusci_a1_ring_buffer_full = (1 == uart_state->uart_config.ring_buffer_size);
		uart_state->eusci_a1_rx_error = 0x00;
		(void)initializeEusciAParametersForSMClkAtBaudRate(&eusci_a1_parameters, uart_state->uart_config.baud_rate);
		if ( false == EUSCI_A_UART_init(EUSCI_A1_BASE, &eusci_a1_parameters) ) {
			result = __LINE__;
			//LogError("uartio failed to initialize the system UART\n");
		} else {
			(void)EUSCI_A_UART_enable(EUSCI_A1_BASE);
			(void)EUSCI_A_UART_enableInterrupt(EUSCI_A1_BASE, UCRXIE);
			//LogError("uartio has initialized the system UART\n");
		}

		uart_state->uartio_open = true;
	}

	if ( on_io_open_complete_ ) { on_io_open_complete_(on_io_open_complete_context_, IO_OPEN_OK); }
	return result;
}


OPTIONHANDLER_HANDLE
uartio_retrieveoptions (
	CONCRETE_IO_HANDLE uartio_
) {
	OPTIONHANDLER_HANDLE result;

	if ( NULL == uartio_ ) {
		result = NULL;
	} else if ( _singleton != uartio_ ) {
		result = NULL;
	} else if ( NULL == (result = OptionHandler_Create(uartio_cloneoption, uartio_destroyoption, uartio_setoption)) ) {
		LogError("Unable to create option handle for uartio\n");
	} else {
		LogInfo("uartio options were successfully retrieved\n");
	}

	return result;
}


int
uartio_send (
	CONCRETE_IO_HANDLE uartio_,
	const void * buffer_,
	size_t buffer_size_,
	ON_SEND_COMPLETE on_send_complete_,
	void * callback_context_
) {
	if ( !_singleton || (_singleton != uartio_) ) { return __LINE__; }
	if ( !buffer_ ) { return __LINE__; }

	UartIoState * uart_state = (UartIoState *)uartio_;

	if ( !uart_state->uartio_open ) { return __LINE__; }

	const uint8_t * byte_buffer = (const uint8_t *)buffer_;

  #pragma diag_push
  /*
   * (ULP 13.1) Detected loop counting up.
   *
   * Recommend loops count down as detecting zeros is easier
   */
  //TODO: Confirm decision or address suppression diagnostic message
  #pragma diag_suppress 1544
	for(; *byte_buffer && buffer_size_ ; ++byte_buffer, --buffer_size_)
  #pragma diag_pop
	{
		(void)EUSCI_A_UART_transmitData(EUSCI_A1_BASE, *byte_buffer);
	}

	if ( on_send_complete_ ) { on_send_complete_(callback_context_, IO_SEND_OK); }
	return 0;
}


int
uartio_setoption (
	CONCRETE_IO_HANDLE uartio_,
	const char * const option_name_,
	const void * const option_value_
) {
	int error;

	if ( NULL == uartio_ ) {
		error =  __LINE__;
	} else if ( _singleton != uartio_ ) {
		error =  __LINE__;
	} else if ( NULL == option_name_ ) {
		error =  __LINE__;
	} else {
		error =  __LINE__;
		LogError("Unable to set unrecognized uartio option \"%s\"\n", option_name_);
	}

	return error;
}


void *
uartio_cloneoption (
	const char * option_name_,
	const void * option_value_
) {
	void * clone;

	if ( NULL == option_name_ ) {
		LogError("No name was passed to uartio_cloneoption\n");
		clone = NULL;
	} else {
		clone = NULL;
		LogError("Unable to destroy unrecognized uartio option \"%s\"\n", option_name_);
	}

	return clone;
}


void
uartio_destroyoption (
	const char * option_name_,
	const void * option_value_
) {
	if ( NULL == option_name_ ) {
		LogError("No name was passed to uartio_destroyoption\n");
	} else if ( NULL == option_value_ ) {
		LogInfo("uartio option \"%s\" is NULL\n", option_name_);
	} else {
		LogError("Unable to destroy unrecognized uartio option \"%s\"\n", option_name_);
	}
}


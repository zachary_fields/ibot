// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef XIO_STREAM_FILTER_H
#define XIO_STREAM_FILTER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef void * XIO_STREAM_FILTER_HANDLE;

typedef struct XioStreamFilterInterface {
	void *
	(*xio_stream_filter_contents) (
		XIO_STREAM_FILTER_HANDLE handle_,
		size_t * content_length_
	);

	XIO_STREAM_FILTER_HANDLE
	(*xio_stream_filter_create) (
		void
	);

	void
	(*xio_stream_filter_destroy) (
		XIO_STREAM_FILTER_HANDLE handle_
	);

	bool
	(*xio_stream_filter_full) (
		XIO_STREAM_FILTER_HANDLE handle_
	);

	int
	(*xio_stream_filter_process_stream) (
		XIO_STREAM_FILTER_HANDLE handle_,
		const uint8_t * io_stream_,
		size_t io_stream_length_
	);

	int
	(*xio_stream_filter_reset) (
		XIO_STREAM_FILTER_HANDLE handle_
	);
} XioStreamFilterInterface;

extern
XIO_STREAM_FILTER_HANDLE
xio_stream_filter_create (
	XioStreamFilterInterface * interface_
);

extern
void *
xio_stream_filter_contents (
	XIO_STREAM_FILTER_HANDLE handle_,
	size_t * content_length_
);

extern
void
xio_stream_filter_destroy (
	XIO_STREAM_FILTER_HANDLE handle_
);

extern
bool
xio_stream_filter_full (
	XIO_STREAM_FILTER_HANDLE handle_
);

extern
int
xio_stream_filter_process_stream (
	XIO_STREAM_FILTER_HANDLE handle_,
	const uint8_t * io_stream_,
	size_t io_stream_length_
);

extern
int
xio_stream_filter_reset (
	XIO_STREAM_FILTER_HANDLE handle_
);

#endif // XIO_STREAM_FILTER_H

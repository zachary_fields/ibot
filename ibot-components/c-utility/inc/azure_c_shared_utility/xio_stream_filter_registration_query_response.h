// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef XIO_STREAM_FILTER_REGISTRATION_QUERY_RESPONSE_H
#define XIO_STREAM_FILTER_REGISTRATION_QUERY_RESPONSE_H

#include <stdint.h>

#include "azure_c_shared_utility/xio_stream_filter.h"

typedef struct RegistrationQueryResponse {
	uint8_t n;
	uint8_t stat;
	uint8_t lac[5];
	uint8_t ci[5];
} RegistrationQueryResponse;

extern
XioStreamFilterInterface *
xio_stream_filter_get_registration_query_response_interface (
	void
);

#endif // XIO_STREAM_FILTER_REGISTRATION_QUERY_RESPONSE_H

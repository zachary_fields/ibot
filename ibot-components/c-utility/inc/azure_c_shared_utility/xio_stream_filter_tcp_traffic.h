// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef XIO_STREAM_FILTER_TCP_TRAFFIC_H
#define XIO_STREAM_FILTER_TCP_TRAFFIC_H

#include "azure_c_shared_utility/xio_stream_filter.h"

extern
XioStreamFilterInterface *
xio_stream_filter_get_tcp_traffic_interface (
	void
);

#endif // XIO_STREAM_FILTER_TCP_TRAFFIC_H

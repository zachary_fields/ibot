#include <msp430.h> 
#include <driverlib.h>

#include "mqtt_client_sample.h"

#define TURBO_BUTTON 0
#define LUDICROUS_SPEED 0

void
msp430_init (
    void
);


/*
 * main.c
 */
int main(void) {
	(void)msp430_init();
	
    mqtt_client_sample_run();
	return 0;
}


//*****************************************************************************
//  This work would be done by the end-user, and the SDK will react accordingly
//*****************************************************************************
void
msp430_init (
    void
) {
    WDTCTL = WDTPW | WDTHOLD;    // Stop watchdog timer
  #if TURBO_BUTTON
   #if LUDICROUS_SPEED
    (void)CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_6);
   #else
    (void)CS_setDCOFreq(CS_DCORSEL_1, CS_DCOFSEL_4);
   #endif
    (void)CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_2);
  #else
    // Default values
    (void)CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_6);
    (void)CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_8);
  #endif

    // Initialize Port A
    PAOUT = 0x00;  // Set Port A to LOW
    PADIR = 0x00;  // Set Port A to INPUT

    // Initialize Port B
    PBOUT = 0x00;  // Set Port B to LOW
    PBDIR = 0x00;  // Set Port B to OUTPUT

    PM5CTL0 &= ~LOCKLPM5;        // Disable the GPIO power-on default high-impedance mode to
                                // activate previously configured port settingsGS (affects RTC)
    __bis_SR_register(GIE);        // Enable Global Interrupt

    return;
}

